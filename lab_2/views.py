from django.http import HttpResponse
from django.core import serializers
from django.shortcuts import render
from lab_2.models import Note

# this method renders HTML for our response
def index(request):
    note = Note.objects.all().values()  # mengambil seluruh objek dari class Note
    response = {'note': note}
    return render(request, 'lab2.html', response)

# this method renders XML for our response
def xml(self):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# this method renders JSON for our response
def json(self):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")