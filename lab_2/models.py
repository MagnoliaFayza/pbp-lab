from django.db import models

# membuat Note model
class Note(models.Model):
    to = models.CharField(max_length=30, default=None)
    From = models.CharField(max_length=30, default=None)
    title = models.CharField(max_length=50, default=None)
    message = models.CharField(max_length=150, default=None)