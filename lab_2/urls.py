from django.urls import path
from .views import index, xml, json

# menambahkan route agar result dapat diakses melalui url localhost
urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]