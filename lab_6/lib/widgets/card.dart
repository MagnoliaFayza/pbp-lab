import 'package:flutter/material.dart';

class CardRegulasi extends StatefulWidget {
  const CardRegulasi({Key? key}) : super(key: key);

  @override
  _CardRegulasiState createState() => _CardRegulasiState();
}

class _CardRegulasiState extends State<CardRegulasi> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(children: [
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      'INFO REGULASI',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Lihat info regulasi negara destinasimu selengkapnya disini!",
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Text(
                          "Created at: Nov. 15, 2021",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        textStyle: TextStyle(fontSize: 17),
                        primary: Colors.grey,
                        onPrimary: Colors.white,
                        side: BorderSide(width: 1, color: Colors.grey),
                        padding: EdgeInsets.only(
                          left: 12, right: 12, top: 12, bottom: 12
                        ),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)
                        ),
                      ),
                      onPressed: () {},
                      child: Text('Lihat selengkapnya'),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
