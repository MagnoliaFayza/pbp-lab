import 'package:flutter/material.dart';

class CountryForm extends StatefulWidget {
  const CountryForm({Key? key}) : super(key: key);

  @override
  _CountryFormState createState() => _CountryFormState();
}

class _CountryFormState extends State<CountryForm> {
  late TextEditingController _controller;
  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text("Info Regulasi",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
            textAlign: TextAlign.center
          ),
          Text("Cari tahu mengenai regulasi Covid-19 negara destinasimu disini!",
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: "Ketik untuk mencari",
                labelText: "Nama negara",
                contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.blue, width: 2.0),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 20),
              primary: Colors.blue,
              onPrimary: Colors.white,
              padding: EdgeInsets.only(left: 10, right: 10, top: 8, bottom: 8),
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)
              ),
            ),
            onPressed: () {},
            child: const Text('Pilih'),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      )
    );
  }
}
