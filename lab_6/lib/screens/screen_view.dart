import 'package:flutter/material.dart';
import 'package:lab_6/widgets/card.dart';
import 'package:lab_6/widgets/country_form.dart';

class RegulasiHome extends StatefulWidget {
  const RegulasiHome({Key? key}) : super(key: key);

  @override
  _RegulasiState createState() => _RegulasiState();
}

class _RegulasiState extends State<RegulasiHome> {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CountryForm(),
          SizedBox(height: 25),
          CardRegulasi(),
          CardRegulasi(),
          CardRegulasi(),
          SizedBox(height: 20,),
        ]
    );
  }
}
