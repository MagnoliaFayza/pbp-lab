from django.urls import path
from .views import index, add_friend

# menambahkan route agar result dapat diakses melalui url localhost
urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add_friend'),
]