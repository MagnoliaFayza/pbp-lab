from django.forms import widgets
from lab_1.models import Friend
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

# membuat FriendForm model
class FriendForm(forms.ModelForm):
    # semua field yg dimiliki friend dijadiin form
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {'dob': DateInput()}
