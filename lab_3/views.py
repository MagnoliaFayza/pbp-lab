from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url = '/admin/login/')

# this method renders HTML for our response
def index(request):
    friends = Friend.objects.all().values()  # mengambil seluruh objek dari class Friend
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = '/admin/login/')

# this method renders HTML for our form
def add_friend(request):
    form = FriendForm(request.POST or None)
    response = {'form': form}
    if (form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3')
    else:
        return render(request, 'lab3_form.html', response)