import 'package:flutter/material.dart';

class MainDrawer extends StatefulWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: new CircleAvatar(
              radius: 50.0,
              backgroundImage: NetworkImage('https://yt3.ggpht.com/ytc/AKedOLRGVmqZzHjDrcdgnHYmw8uyDUc-p4uJa-YiptR2dw=s900-c-k-c0x00ffffff-no-rj'),
            ),
            accountName: Text("Magnolia Fayza"),
            accountEmail: Text("magnolia.fayza01@ui.ac.id"),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData : Icons.insert_chart,
            title: "Info Statistik",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.fact_check,
            title: "Regulasi",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.health_and_safety,
            title: "Get Swabbed!",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.hotel,
            title: "Info Hotel Karantina",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.article,
            title: "Artikel",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.help,
            title: "Bantuan",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.arrow_back_rounded,
            title: "Log out",
            onTilePressed: () {
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData ;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: TextStyle(fontSize: 16),),
    );
  }
}