from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend

mhs_name = 'Magnolia Fayza'  # nama
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 11, 30)  # tanggal lahir
npm = 2006596900  # npm


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().values()  # mengambil seluruh objek dari class Friend
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
