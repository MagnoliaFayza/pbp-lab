from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    # menambahkan friends path menggunakan friend_list Views
    path('friends', friend_list, name='friends'),
]
