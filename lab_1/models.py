from django.db import models

# membuat Friend model
class Friend(models.Model):
    # menambahkan atribut di Friend model
    name = models.CharField(max_length=30, default=None)
    npm = models.IntegerField(default=None)
    dob = models.DateField(default=None)