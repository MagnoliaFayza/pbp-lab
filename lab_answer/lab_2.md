1. Apakah perbedaan antara JSON dan XML?
- JSON (JavaScript Object Notation) hanyalah format data sedangkan XML (Extensible Markup Language) adalah bahasa markup
- JSON adalah format pertukaran data ringan yang jauh lebih mudah bagi komputer untuk mengurai data yang sedang dikirim,
  sementara XML digunakan untuk menyimpan dan mengangkut data dari satu aplikasi ke aplikasi lain melalui internet
- Format JSON lebih mudah dibaca daripada format XML
- Data JSON disimpan seperti map dengan pasangan key dan value, sementara XML disimpan sebagai tree structure
- JSON tidak melakukan pemrosesan/perhitungan apapun, sementara XML dapat melakukan pemrosesan dan pemformatan dokumen dan objek
- JSON sangat cepat karena ukuran file sangat kecil dan penguraian lebih cepat oleh mesin JavaScript sehingga transfer data lebih cepat,
  sedangkan XML ukurannya besar dan lambat dalam penguraian sehingga transmisi data menjadi lebih lambat
- Tidak ada ketentuan untuk namespace, menambahkan komentar, atau menulis metadata pada JSON, sementara XML sebaliknya
- JSON hanya mendukung string, angka, array Boolean, dan objek (hanya dapat bertipe primitif). Sementara itu,
  XML mendukung banyak tipe data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya
- Penguraian JSON hampir selalu aman kecuali jika JSONP digunakan karena dapat menyebabkan serangan Cross-Site Request Forgery (CSRF),
  sementara XML rentan terhadap beberapa serangan karena perluasan entitas eksternal dan validasi DTD diaktifkan secara default sehingga
  lebih baik dinonaktifkan agar pengurai XML bisa lebih aman
- Menggunakan JSON dengan AJAX memastikan pemrosesan yang lebih cepat karena data diproses secara serial, sedangkan
  banyak bandwidth yang dikonsumsi secara tidak perlu akibat tag di XML sehingga permintaan AJAX menjadi lambat
- JSON lebih baik digunakan apabila memiliki pertukaran data sederhana yang tidak perlu repot tentang semantik dan validasi

2. Apakah perbedaan antara HTML dan XML?
- HTML (Hypertext Markup Language) memiliki fokus kepada penyajian data, sementara
  XML (Extensible Markup Language) memiliki fokus kepada transfer data
- HTML tidak bersifat case sensitive, sementara XML iya
- HTML tidak terlalu ketat dalam memerhatikan tag penutup, sementara XML sangat ketat
- HTML memiliki tag yang sedikit terbatas, sementara tag XML biasanya dapat dikembangkan kembali
- HTML memiliki memiliki tag yang sudah ditentukan sebelumnya, sementara XML tidak
- HTML membantu mengembangkan struktur halaman web, sementara XML membantu untuk bertukar data di antara berbagai platform
- Tidak apa-apa jika ada error kecil pada HTML, sementara pada XML tidak boleh ada error
